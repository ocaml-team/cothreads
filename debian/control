Source: cothreads
Section: ocaml
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders:
 Mehdi Dogguy <mehdi@debian.org>,
 Erik de Castro Lopo <erikd@mega-nerd.com>
Build-Depends:
 debhelper-compat (= 13),
 ocaml (>= 5.1),
 dh-ocaml
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: http://sourceforge.net/projects/cothreads/
Vcs-Git: https://salsa.debian.org/ocaml-team/cothreads.git
Vcs-Browser: https://salsa.debian.org/ocaml-team/cothreads

Package: libcothreads-ocaml-dev
Architecture: any
Depends: ${ocaml:Depends}, ${misc:Depends}
Provides: ${ocaml:Provides}
Recommends: ocaml-findlib
Description: concurrent programming library for OCaml
 This library enhances the Threads library of the standard OCaml distribution
 in two dimensions:
 .
 - It implements the same API of the standard Threads library on different
 execution engines (process, networker), so that a single copy of
 source code can be compiled and deployed to different environments
 without modification.
 - It is also a super set of the standard Threads library, with extra
 components (STM etc.), functions (spawn etc.) and features (object-level
 compatibility etc.).
 .
 This package provides static libraries, interfaces, and documentation
 for coThreads.
